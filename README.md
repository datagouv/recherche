# recherche.etalab.studio 

## Running the server

    python3 -m http.server


## Generating data

    ./run.py generate-data


## Running tests

    pip install pytest
    pytest tests.py -x --disable-warnings
